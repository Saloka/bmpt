#include "worm.h"

//Defines variables
worm_t worm;
point_t eat;
int8_t display[GAME_SIZE_X][GAME_SIZE_Y];

/*Functions*/
uint8_t eat_check(void)
{
    int8_t problem_flag = 0;

    for (int8_t i = 0; i < worm.len; i++)
    {
        //Prevents generating food into snake
        if (worm.cell[i].x == eat.x && worm.cell[i].y == eat.y)
        {
            problem_flag = 1;
            break;
        }
    }
    return problem_flag;
}

/*Generates food on random position*/
void eat_new(void)
{
    do
    {
        eat.x = TCNT1 % GAME_SIZE_X;      //Timer counter1 register dividing
        eat.y = (TCNT1 >> 1) % GAME_SIZE_Y; //Bit shifting of
                                            //register to the right
    }
    while (eat_check());    //Generates food except on snakes positions
}

/*Sets start positions of snake and food*/
void game_init(void)
{
    worm.len = 4;                   //Sets snakes starting length
    worm.is_collision = 0;          //Colision flag cleard
    worm.direction    = RIGHT;      //First move to the right

    //Sets snakes starting position
    int8_t x_start = 6;
    int8_t y_start = 2;

    //Sets starting positions of body to the right
    for (int8_t i = 0; i < worm.len; i++)
    {
        worm.cell[i].x = worm.len - i - 1 + x_start;
        worm.cell[i].y = y_start;
    }

    eat_new();    //Generates new food
}

//Stores positions of snake and food into memory with delay 250ms
void game_update(void)
{
    //Clears whole display with constant CHAR_SPACE
    for (int8_t x = 0; x < GAME_SIZE_X; x++)
        for (int8_t y = 0; y < GAME_SIZE_Y; y++)
            display[x][y] = CHAR_SPACE;
    //Display head and body
    for (int8_t i = 0; i < worm.len; i++)
        display[worm.cell[i].x][worm.cell[i].y] = (i) ? CHAR_BODY : CHAR_HEAD;
        //If it is 0, display head, else display body

    display[eat.x][eat.y] = CHAR_EAT;     //Display food
}

/*Display snake and food*/
void game_show(void)
{
    //Sets cursor starting positions
    lcd_gotoxy(0, 0);

    //Display END GAME screen if .is_collision is 1
    if (worm.is_collision)
    {
        lcd_puts("################");
        lcd_gotoxy(0, 1);
        lcd_puts("# GAME         #");
        lcd_gotoxy(0, 2);
        lcd_puts("#        OVER! #");
        lcd_gotoxy(0, 3);
        lcd_puts("################");
    }
    else
    {
        //Updates display
        game_update();
        //Display snake and food every 250ms
        for (int8_t y = 0; y < GAME_SIZE_Y; y++)
        {
            lcd_gotoxy(0, y);     //Sets cursor x position to 0
            for (int8_t x = 0; x < GAME_SIZE_X; x++)
                lcd_putc(display[x][y]);
        }
    }
}

void game_move(void)
{
    point_t end;    //Declares point_t type variable
    //Defines end variable
    end.x = worm.cell[worm.len - 1].x;
    end.y = worm.cell[worm.len - 1].y;

    //Stores old position into actual
    for (int8_t i = worm.len - 1; i; i--)
    {
        worm.cell[i].x = worm.cell[i - 1].x;
        worm.cell[i].y = worm.cell[i - 1].y;
    }

    //Defines direction of movement
    switch (worm.direction)
    {
        //Move up, increase y by 1
        case TOP:
            if (worm.cell[0].y + 1 < GAME_SIZE_Y)
                ++worm.cell[0].y;
            else
                worm.is_collision = 1;
            break;
        //Move down, decrease y by 1
        case DOWN:
            if (worm.cell[0].y - 1 >= 0)
                --worm.cell[0].y;
            else
                worm.is_collision = 1;
            break;
        //Move left, decrease x by 1
        case LEFT:
            if (worm.cell[0].x - 1 >= 0)
                --worm.cell[0].x;
            else
                worm.is_collision = 1;
            break;
        //Move right, increase x by 1
        default:
            if (worm.cell[0].x + 1 < GAME_SIZE_X)
                ++worm.cell[0].x;
            else
                worm.is_collision = 1;
            break;
    }

    //Checks if head hited body (-1 END GAME)
    for (int8_t i = 1; i < worm.len; i++)
    {
        if (worm.cell[0].x == worm.cell[i].x && worm.cell[0].y == worm.cell[i].y)
        {
            worm.is_collision = 1;
            break;
        }
    }

    //Checks if snake ate food
    if (worm.cell[0].x == eat.x && worm.cell[0].y == eat.y && worm.len < MAX_WORM_LEN)
    {
        worm.cell[worm.len].x = end.x;
        worm.cell[worm.len].y = end.y;
        ++worm.len;
        eat_new();
    }
} /* game_move */
