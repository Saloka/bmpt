/**
 ****************************************************************************
 * @file    main.c
 * @author  Sekanina Jíří, Brno University of Technology, Czechia
 * @version V1.0
 * @date    Dec 1, 2018
 * @brief   Template for Snake game on LCD display.
 * @note    Modified version of Peter Fleury's LCD library with R/W pin
 *          connected to GND. Newline symbol "\n" is not implemented, use
 *          lcd_gotoxy() function instead.
 ****************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "settings.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "worm.h"
#include "lcd.h"

void setup(void)
{
    uint8_t i;

    // The BitMap for the head
    uint8_t HEAD[8] = {
        0b10001,
        0b01010,
        0b11111,
        0b10101,
        0b11111,
        0b10101,
        0b10001,
        0b01110
    };

    // The BitMap for the body
    uint8_t BODY[8] = {
        0b00100,
        0b01110,
        0b01110,
        0b11011,
        0b11011,
        0b01110,
        0b01110,
        0b00100
    };

    // The BitMap for Food
    uint8_t FOOD[8] = {
        0b00000,
        0b00000,
        0b01000,
        0b00100,
        0b01110,
        0b11011,
        0b11110,
        0b01100
    };


    /* Initialize display */
    lcd_init(LCD_DISP_ON);

    /* Set pointer to beginning of CG RAM memory */
    lcd_command(1 << LCD_CGRAM);
    /* Store Head character*/
    for (i = 0; i < 8; i++)
        lcd_data(HEAD[i]);

    /* Store Body character*/
    for (i = 0; i < 8; i++)
        lcd_data(BODY[i]);

    /* Store Food character*/
    for (i = 0; i < 8; i++)
        lcd_data(FOOD[i]);

    /* Clear display */
    lcd_clrscr();

    /* Timer/Counter1: start Analog-to-Digital conversion */
    /* No clock prescaler */
    TCCR1B |= _BV(CS10);// _BV(CS11) | _BV(CS10);
    /* Overflow interrupt enable */
    TIMSK1 |= _BV(TOIE1);

    /*Allow interrupts*/
    sei();
} /* setup */

/*main function*/
int main(void)
{
    setup();
    game_init();    //Store beginning positions of snake and food

    while (1)
    {
        game_show();    //Shows characters on display
        game_move();    //Moves with snake and generater food
        _delay_ms(250); //Delay for move
    }

    return 0;
}
