#include "settings.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "worm.h"

//interruption
ISR(TIMER1_OVF_vect){
    // Set rows to log0
    DDRC &= ~_BV(PC2);
    DDRC &= ~_BV(PC3);
    DDRC &= ~_BV(PC4);
    DDRC &= ~_BV(PC5);
    //Set pullup resitors
    PORTC |= _BV(PC2);
    PORTC |= _BV(PC3);
    PORTC |= _BV(PC4);
    PORTC |= _BV(PC5);

    // Set columns
    DDRC |= _BV(PC0);
    DDRC |= _BV(PC1);
    DDRB |= _BV(PB2);
    DDRB |= _BV(PB3);

    // Set log0 to Col 1, others to log1
    PORTB &= ~_BV(PB3);
    PORTB |= _BV(PB2);
    PORTC |= _BV(PC0);
    PORTC |= _BV(PC1);
    _delay_us(100);

    if (bit_is_clear(PINC, 3))
    {
        worm.direction = LEFT;    //Set direction to Left if there
    }                             //is log0 on pinC3 (key4)


    // Set log0 to Col 2, others to log1
    PORTB |= _BV(PB3);
    PORTB &= ~_BV(PB2);
    PORTC |= _BV(PC0);
    PORTC |= _BV(PC1);
    _delay_us(100);

    if (bit_is_clear(PINC, 2))
    {
        worm.direction = DOWN;    //Set direction to Down if there
    }                             //is log0 on pinC2 (key8)

    if (bit_is_clear(PINC, 4))
    {
        worm.direction = TOP;     //Set direction to Top if there
    }                             //is log0 on pinC4 (key2)

    // Set log0 to Col 3, others to log1
    PORTB |= _BV(PB3);
    PORTB |= _BV(PB2);
    PORTC &= ~_BV(PC0);
    PORTC |= _BV(PC1);
    _delay_us(100);

    if (bit_is_clear(PINC, 3))
    {
        worm.direction = RIGHT;   //Set direction to Right if there
    }                             //is log0 on pinC3 (key6)
}
