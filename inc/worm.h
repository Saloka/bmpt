#ifndef WORM_H
#define WORM_H

#include <avr/io.h>
#include "lcd.h"

#define GAME_SIZE_X  16     //Sets x size of display
#define GAME_SIZE_Y  4      //Sets y size of display
#define MAX_WORM_LEN GAME_SIZE_X * GAME_SIZE_Y    //Sets maximum length of snake

#define CHAR_EAT     2      //Defines CHAR_EAT as second sign from main.c
#define CHAR_HEAD    0      //Defines CHAR_HEAD as second sign from main.c
#define CHAR_BODY    1      //Defines CHAR_BODY as second sign from main.c
#define CHAR_SPACE   ' '    //Defines empty sign

//Defines all directions in data type direction_t
typedef enum {
    TOP,
    DOWN,
    LEFT,
    RIGHT
} direction_t;

//Defines x, y to structure point_t
typedef struct {
    int8_t x;
    int8_t y;
} point_t;

//Defines variables to structure worm_t
typedef struct {
    uint8_t     len;
    direction_t direction;
    point_t     cell[MAX_WORM_LEN];
    uint8_t     is_collision;
} worm_t;

//Use extern variables
extern worm_t worm;
extern point_t eat;
extern int8_t display[GAME_SIZE_X][GAME_SIZE_Y];

//List of used functions in worm.c
uint8_t eat_check(void);
void eat_new(void);
void game_init(void);
void game_update(void);
void game_show(void);
void game_move(void);

#endif // ifndef WORM_H
